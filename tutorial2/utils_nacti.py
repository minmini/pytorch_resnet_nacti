import torch
import torchvision
import torchvision.transforms as transforms
import matplotlib.pyplot as plt
import numpy as np

    classes = ('1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
              '11', '12', '13', '14', '15', '16', '17', '18', '19', '20',
              '21', '22', '23', '24', '25', '26', '27', '28', '29', '30',
              '31', '32', '33', '34', '35', '36', '37', '38', '39', '40',
              '41', '42', '43', '44', '45', '46', '47', '48', '49', '50','51') 


def _get_transform():    
    return transforms.Compose([transforms.Resize(255),
                                 transforms.CenterCrop(224),
                                 transforms.ToTensor()])
def get_train_data_loader():
    transform = _get_transform()
    import tarfile
    my_tar = tarfile.open('data/train.tar')
    my_tar.extractall('./train') # specify which folder to extract to
    my_tar.close
    trainset = torchvision.datasets.ImageFolder('./train', transform=transform)
    return torch.utils.data.DataLoader(trainset, batch_size=4,
                                          shuffle=True, num_workers=2)

    
def get_test_data_loader():
    transform = _get_transform()
    import tarfile
    my_tar = tarfile.open('data/test.tar')
    my_tar.extractall('./test') # specify which folder to extract to
    my_tar.close
    trainset = torchvision.datasets.ImageFolder('./test', transform=transform)
    return torch.utils.data.DataLoader(trainset, batch_size=4,
                                          shuffle=True, num_workers=2)    

# function to show an image
def imshow(img):
#    img = img / 2 + 0.5     # unnormalize
    npimg = img.numpy()
    plt.imshow(np.transpose(npimg, (1, 2, 0)))