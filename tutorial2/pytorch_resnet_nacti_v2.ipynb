{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Advanced NACTI Training using PyTorch"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Contents\n",
    "\n",
    "1. [Background](#Background)\n",
    "1. [Setup](#Setup)\n",
    "1. [Data](#Data)\n",
    "1. [Train](#Train)\n",
    "1. [Host](#Host)\n",
    "\n",
    "---\n",
    "\n",
    "## Background\n",
    "\n",
    "Horovod is a distributed deep learning training framework for TensorFlow, Keras, PyTorch, and MXNet. This notebook example shows how to perform distributed training using Horovod with PyTorch in SageMaker using NACTI dataset.\n",
    "\n",
    "For more information about the PyTorch in SageMaker, please visit [sagemaker-pytorch-containers](https://github.com/aws/sagemaker-pytorch-containers) and [sagemaker-python-sdk](https://github.com/aws/sagemaker-python-sdk) github repositories.\n",
    "\n",
    "---\n",
    "\n",
    "## Setup\n",
    "\n",
    "_This notebook was created and tested on an ml.p2.xlarge notebook instance._\n",
    "\n",
    "Let's start by creating a SageMaker session and specifying:\n",
    "\n",
    "- The S3 bucket and prefix that you want to use for training and model data.  This should be within the same region as the Notebook Instance, training, and hosting.\n",
    "- The IAM role arn used to give training and hosting access to your data. See the [Amazon SageMaker Roles](https://docs.aws.amazon.com/sagemaker/latest/dg/sagemaker-roles.html) for how to create these.  Note, if more than one role is required for notebook instances, training, and/or hosting, please replace the `sagemaker.get_execution_role()` with the appropriate full IAM role arn string(s).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sagemaker\n",
    "sagemaker_session = sagemaker.Session()\n",
    "bucket = \"nacti\" \n",
    "prefix = \"tutorial2\"\n",
    "role = sagemaker.get_execution_role()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Data\n",
    "### Getting the data\n",
    "\n",
    "In this example, we will use the NACTI dataset. NACTI is a wildlife dataset for containing camera trap images from North America. It consists of over 3 million labelled images. We have left out two million images of domestic cows and empty images (images having no wildlife) and have created a dataset with 900,000 training images and 100,000 test images. There are 51 classes (one for each of the 51 anilams in the NACTI dataset.).\n",
    "\n",
    "**Please note**\n",
    "\n",
    "This cell needs to be executed only the very first time the notebook is run. Once the data is downloaded to the notebook instance, you can save time by avoiding repeated download of the same data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#!wget https://nacti.s3.amazonaws.com/tutorial2/train.tar -O data/train.tar\n",
    "!wget https://nacti.s3.amazonaws.com/tutorial2/test.tar -O data/test.tar"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Upload the data\n",
    "We use the ```sagemaker.Session.upload_data``` function to upload our datasets to an S3 location. The return value inputs identifies the location -- we will use this later when we start the training job.\n",
    "\n",
    "**Please note**\n",
    "\n",
    "Once the data is uploaded to S3 once, you can save time by avoiding repeated upload of the same data. For subsequent training runs, just hardcode the S3 folder location obtained as the outout of the first run."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "inputs = sagemaker_session.upload_data(path='data', bucket=bucket, key_prefix='tutorial2')\n",
    "#inputs = <hardcode inputs string here>\n",
    "print(inputs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Train\n",
    "### Training script\n",
    "The `mnist.py` script provides the code we need for training a SageMaker model.\n",
    "The training script is very similar to a training script you might run outside of SageMaker, but you can access useful properties about the training environment through various environment variables, such as:\n",
    "\n",
    "* `SM_MODEL_DIR`: A string representing the path to the directory to write model artifacts to.\n",
    "  These artifacts are uploaded to S3 for model hosting.\n",
    "* `SM_NUM_GPUS`: The number of gpus available in the current container.\n",
    "* `SM_CURRENT_HOST`: The name of the current container on the container network.\n",
    "* `SM_HOSTS`: JSON encoded list containing all the hosts .\n",
    "\n",
    "Supposing one input channel, 'training', was used in the call to the PyTorch estimator's `fit()` method, the following will be set, following the format `SM_CHANNEL_[channel_name]`:\n",
    "\n",
    "* `SM_CHANNEL_TRAINING`: A string representing the path to the directory containing data in the 'training' channel.\n",
    "\n",
    "For more information about training environment variables, please visit [SageMaker Containers](https://github.com/aws/sagemaker-containers).\n",
    "\n",
    "A typical training script loads data from the input channels, configures training with hyperparameters, trains a model, and saves a model to `model_dir` so that it can be hosted later. Hyperparameters are passed to your script as arguments and can be retrieved with an `argparse.ArgumentParser` instance.\n",
    "\n",
    "This script uses Horovod framework for distributed training where Horovod-related lines are commented with `Horovod:`. For example, `hvd.broadcast_parameters`, `hvd.DistributedOptimizer` and etc.\n",
    "\n",
    "For example, the script run by this notebook:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Run training in SageMaker\n",
    "\n",
    "The `PyTorch` class allows us to run our training function as a training job on SageMaker infrastructure. We need to configure it with our training script, an IAM role, the number of training instances, the training instance type, and hyperparameters. In this case we are going to run our training job on 2 ```ml.p2.xlarge``` instances. But this example can be ran on one or multiple, cpu or gpu instances ([full list of available instances](https://aws.amazon.com/sagemaker/pricing/instance-types/)). The hyperparameters parameter is a dict of values that will be passed to your training script -- you can see how to access these values in the `nacti.py` script in the code folder.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sagemaker.pytorch import PyTorch\n",
    "\n",
    "estimator = PyTorch(\n",
    "    entry_point=\"nacti.py\",\n",
    "    source_dir=\"code\",\n",
    "    role=role,\n",
    "    py_version=\"py3\",\n",
    "    framework_version=\"1.8.1\",\n",
    "    instance_count=5,\n",
    "    max_run = 187200,\n",
    "    volume_size=200,\n",
    "    instance_type= \"ml.g4dn.2xlarge\",\n",
    "    distribution={\n",
    "        \"mpi\": {\n",
    "            \"enabled\": True,\n",
    "            \"processes_per_host\": 1,\n",
    "            \"custom_mpi_options\": \"--NCCL_DEBUG INFO\"\n",
    "        },\n",
    "    },\n",
    "    hyperparameters={\"epochs\": 2, \"backend\": \"nccl\", \"lr\":0.04,\"batch-size\":150},\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After we've constructed our `PyTorch` object, we can fit it using the data we uploaded to S3. SageMaker makes sure our data is available in the local filesystem, so our training script can simply read the data from disk.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "estimator.fit({\"training\": inputs})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Host\n",
    "### Create endpoint\n",
    "After training, we need to use the `PyTorch` estimator object to create a `PyTorchModel` object and set a different `entry_point`, otherwise, the training script `nacti.py` will be used for inference. (Note that the new `entry_point` must be under the same `source_dir` as `nacti.py`). Then we use the `PyTorchModel` object to deploy a `PyTorchPredictor`. This creates a Sagemaker Endpoint -- a hosted prediction service that we can use to perform inference.\n",
    "\n",
    "An implementation of `model_fn` is required for inference script. We are going to use default implementations of `input_fn`, `predict_fn`, `output_fn` and `transform_fm` defined in [sagemaker-pytorch-containers](https://github.com/aws/sagemaker-pytorch-containers).\n",
    "\n",
    "Here's an example of the inference script:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The arguments to the deploy function allow us to set the number and type of instances that will be used for the Endpoint. These do not need to be the same as the values we used for the training job.  Here we will deploy the model to a single ```ml.m4.xlarge``` instance."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a PyTorchModel object with a different entry_point\n",
    "model = estimator.create_model(entry_point=\"inference.py\", source_dir=\"code\")\n",
    "# Deploy the model to a ml.m4.xlarge instance\n",
    "predictor = model.deploy(initial_instance_count=1, instance_type=\"ml.m4.xlarge\")\n",
    "from utils_nacti import get_train_data_loader, get_test_data_loader, imshow, classes\n",
    "testloader = get_test_data_loader()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Evaluate\n",
    "We can now use this predictor to classify random images from our test set. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import torchvision, torch\n",
    "# get some test images\n",
    "dataiter = iter(testloader)\n",
    "images, labels = dataiter.next()\n",
    "\n",
    "# print images\n",
    "imshow(torchvision.utils.make_grid(images))\n",
    "print('GroundTruth: ', ' '.join('%4s' % classes[labels[j]] for j in range(4)))\n",
    "\n",
    "outputs = predictor.predict(images.numpy())\n",
    "\n",
    "_, predicted = torch.max(torch.from_numpy(np.array(outputs)), 1)\n",
    "\n",
    "print('Predicted: ', ' '.join('%4s' % classes[predicted[j]]\n",
    "                              for j in range(4)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Cleanup\n",
    "\n",
    "After you have finished with this example, remember to delete the prediction endpoint to release the instance(s) associated with it"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "predictor.delete_endpoint()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "conda_pytorch_p36",
   "language": "python",
   "name": "conda_pytorch_p36"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.13"
  },
  "notice": "Copyright 2020 Amazon.com, Inc. or its affiliates. All Rights Reserved.  Licensed under the Apache License, Version 2.0 (the \"License\"). You may not use this file except in compliance with the License. A copy of the License is located at http://aws.amazon.com/apache2.0/ or in the \"license\" file accompanying this file. This file is distributed on an \"AS IS\" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License."
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
