import torch
import torchvision
import torchvision.transforms as transforms
import matplotlib.pyplot as plt
import numpy as np

classes = ('american_marten', 'black_tailed_jackrabbit', 'domestic_dog', 'north_american_porcupine', 'north_american_river_otter', 'unidentified_accipitrid', 'unidentified_corvus', 'unidentified_pack_rat', 'wolf', 'yellow_bellied_marmot')


def _get_transform():    
    return transforms.Compose([transforms.Resize(255),
                                 transforms.CenterCrop(224),
                                 transforms.ToTensor()])
def get_train_data_loader():
    transform = _get_transform()
    import tarfile
    my_tar = tarfile.open('data/train.tar')
    my_tar.extractall('./train') # specify which folder to extract to
    my_tar.close
    trainset = torchvision.datasets.ImageFolder('./train', transform=transform)
    return torch.utils.data.DataLoader(trainset, batch_size=4,
                                          shuffle=True, num_workers=2)

    
def get_test_data_loader():
    transform = _get_transform()
    import tarfile
    my_tar = tarfile.open('data/test.tar')
    my_tar.extractall('./test') # specify which folder to extract to
    my_tar.close
    trainset = torchvision.datasets.ImageFolder('./test', transform=transform)
    return torch.utils.data.DataLoader(trainset, batch_size=4,
                                          shuffle=True, num_workers=2)    

# function to show an image
def imshow(img):
#    img = img / 2 + 0.5     # unnormalize
    npimg = img.numpy()
    plt.imshow(np.transpose(npimg, (1, 2, 0)))